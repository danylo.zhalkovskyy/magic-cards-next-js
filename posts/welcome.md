---
title: "Discover Magic Cards"
date: "June 24 2022"
---

[Magic: The Assembly](https://en.wikipedia.org/wiki/Magic:_The_Gathering) is a card game to play and collect invented by Richard Garfield in 1993. It is often cited as the reference game in the world of playing card games.

* The [API](https://api.magicthegathering.io/v1/cards) used.
