# Magic Cards - Project

This is a [Next.js](https://nextjs.org/) project created with [`$ npx create-next-app@latest`](https://nextjs.org/docs/api-reference/create-next-app).

## Description

> [Magic: The Assembly](https://en.wikipedia.org/wiki/Magic:_The_Gathering) is a card game to play and collect invented by Richard Garfield in 1993. It is often cited as the reference game in the world of playing card games.


The [API](https://api.magicthegathering.io/v1/cards) used.

## Commands

Install the dependencies of the project (package.json):

```bash
$ npm install
```

Run the development server:

```bash
$ npm run dev
```

Build the project:

```bash
$ npm run build
```

Run the project in production mode (apply after build):

```bash
$ npm run start
```

Lint the project, find and fix problems in code:

```bash
$ npm run lint
```

## Licence

[Common Development and Distribution License 1.0](Common Development and Distribution License 1.0)
