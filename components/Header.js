import Link from "next/link";
import styles from '../styles/Header.module.css';

export default function Header() {
    return(
        <div className={styles.headerContent}>
            <Link href="/">
                <a>
                    <h3>Home</h3>
                </a>
            </Link>
            <Link href="/cards">
                <a>
                    <h3>Cards</h3>
                </a>
            </Link>
            <Link href="/licence">
                <a>
                    <h3>Licence</h3>
                </a>
            </Link>
        </div>
    )
}
