import fs from "fs";
import path from "path"
import html from "remark-html";
import {remark} from "remark";
import styles from '../styles/Home.module.css';

export default function Home({ markdownHtml }) {
    return (
        <>
            <div className={styles.homeContent}>
                <h1>Welcome!</h1>
                <div dangerouslySetInnerHTML={{ __html: markdownHtml }} />
            </div>
        </>
    )
}

export async function getStaticProps({ params }) {
    const markdownPath = path.join(process.cwd(), '/posts/welcome.md');
    const markDownFileContent = fs.readFileSync(markdownPath, "utf8");
    const markDownContent = await remark().use(html).process(markDownFileContent);

    return {
        props: {
            markdownHtml: markDownContent.toString(),
        }
    };
}
