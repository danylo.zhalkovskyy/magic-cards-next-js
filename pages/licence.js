import fs from "fs";
import path from "path"
import html from "remark-html";
import {remark} from "remark";
import styles from "../styles/Licence.module.css";

export default function Licence({ markdownHtml }) {
    return (
        <>
            <div className={styles.licenceContent}>
                <div dangerouslySetInnerHTML={{ __html: markdownHtml }} />
            </div>
        </>
    )
}

export async function getStaticProps({ params }) {
    const markdownPath = path.join(process.cwd(), '/posts/licence.md');
    const markDownFileContent = fs.readFileSync(markdownPath, "utf8");
    const markDownContent = await remark().use(html).process(markDownFileContent);

    return {
        props: {
            markdownHtml: markDownContent.toString(),
        }
    };
}
