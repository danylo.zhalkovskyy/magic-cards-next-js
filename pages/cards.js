import axios from "axios";
import Link from "next/link";
import styles from '../styles/Cards.module.css';

export async function getStaticProps() {
    const { data } = await axios.get("https://api.magicthegathering.io/v1/cards");

    return {
        props: {
            cards: data.cards
        }
    };
}

export default function Home({ cards }) {

    const filterCards = (obj) => {
        return obj.imageUrl !== undefined && typeof (obj.imageUrl) === 'string';
    }

    const filteredCards = cards.filter(filterCards)

    return (
        <div className={styles.cardsBox}>
            {filteredCards.map(({ id, imageUrl }) => (
                <div key={id} className={styles.cardSimple}>
                    <Link href={`/cards/${id}`}>
                        <a>
                            <img src={imageUrl} alt="" />
                        </a>
                    </Link>
                </div>
            ))}
        </div>
    );
}
